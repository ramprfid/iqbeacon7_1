﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iqBeaconv7_1.ReaderLayer.Tag
{
    public class RampActiveTag : IRampActiveTag
    {
        public uint SerialNumber { get; set; }

        public RampBatteryStatus Battery { get; set; }

        public int BeaconCounterHighByte { get; set; }

        public int BeaconCounterLowByte { get; set; }

        public DateTime TimeFirstSeen { get; set; }

        public DateTime TimeLastSeen { get; set; }

        public int NewLoopId { get; set; }

        public DateTime NewLoopTime { get; set; }

        public int OldLoopId { get; set; }

        public DateTime OldLoopTime { get; set; }

        public DateTime FirstReadTime { get; set; }

        public DateTime LastConfirmedReadTime { get; set; }

        public bool IsTagArriveMessageSent { get; set; }

        public bool IsTagDepartMessageSent { get; set; }

        public bool IsTagLeftMarkerZone { get; set; }

        public string ToCsv()
        {
            return string.Format("TagId : {0}, OldLoopID : {1}, NewLoopId  {2}, OldLoopTime {3}, NewLoopTime{4}, TimeFirstSeen{5}, TimeLastSeen{6}, Battery {7}, BeaconCounterHighByte{8}, BeaconCounterLowByte{9}",
                SerialNumber, OldLoopId, NewLoopId, OldLoopTime, NewLoopTime, TimeFirstSeen, TimeLastSeen, Battery, BeaconCounterHighByte, BeaconCounterLowByte);
        }

    }
}
