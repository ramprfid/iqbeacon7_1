﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace iqBeaconv7_1.ReaderLayer.Tag
{
    public interface IRampActiveTag
    {
        uint SerialNumber { get; set; }

        RampBatteryStatus Battery { get; set; }

        int BeaconCounterHighByte { get; set; }

        int BeaconCounterLowByte { get; set; }

        DateTime TimeFirstSeen { get; set; }

        DateTime TimeLastSeen { get; set; }

        int NewLoopId { get; set; }
        
        DateTime NewLoopTime { get; set; }
        
        int OldLoopId { get; set; }
        
        DateTime OldLoopTime { get; set; }

        DateTime FirstReadTime { get; set; }

        DateTime LastConfirmedReadTime { get; set; }

        bool IsTagArriveMessageSent { get; set; }

        bool IsTagDepartMessageSent { get; set; }

        bool IsTagLeftMarkerZone { get; set; }

        string ToCsv();

    }
}
