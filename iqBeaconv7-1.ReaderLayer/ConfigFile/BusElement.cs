﻿/* Name - BusElement
 * Author - Puru Karki
 * Purpose - Provide Element Level Access for Bus Configuration File
 * Created Date - 15/10/2014
*/

using System.Configuration;
using iqBeaconv7_1.ReaderLayer.Connection;

namespace iqBeaconv7_1.ReaderLayer.ConfigFile
{
    public class BusElement : ConfigurationElement, IBusConfig
    {
        /// <summary>
        /// Get Set ip address of the bus
        /// </summary>
        [ConfigurationProperty("ip", IsKey = true, IsRequired = true)]
        public string Ip
        {
            get { return (string)this["ip"]; }
            set { this["ip"] = value; }
        }

        /// <summary>
        /// get Set port of the bus
        /// </summary>
        [ConfigurationProperty("port", IsKey = false, IsRequired = true)]
        public int Port
        {
            get { return (int)this["port"]; }
            set { this["port"] = value; }
        }

        /// <summary>
        /// Get Set description of the bus [non-mandatory]
        /// </summary>
        [ConfigurationProperty("description", IsKey = false, IsRequired = false)]
        public string Description
        {
            get { return (string)this["description"]; }
            set { this["description"] = value; }
        }

    }
}
