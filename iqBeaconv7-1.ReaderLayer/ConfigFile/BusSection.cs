﻿/* Name - BusSection
 * Author - Puru Karki
 * Purpose - Provide custom configuration section for Bus file
 * Created Date - 15/10/2014
*/
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iqBeaconv7_1.ReaderLayer.ConfigFile
{
    public class BusSection : ConfigurationSection
    {
        [ConfigurationProperty("busCollection", IsRequired = true, IsDefaultCollection = true)]
        [ConfigurationCollection(typeof(BusCollection), AddItemName = "bus")]
        public BusCollection Bus
        {
            get { return (BusCollection) this["busCollection"]; }
            set { this["busCollection"] = value; }
        }
    }
}
