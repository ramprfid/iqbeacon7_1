﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using iqBeaconv7_1.LogicLayer.LogicBlock;
using NUnit.Framework;
using iqBeaconv7_1.LogicLayer;
using System.Collections.Concurrent;
using iqBeaconv7_1.ReaderLayer.Tag;
namespace iqBeaconv7_1.LogicLayerTest
{
    [TestFixture]
    public class LogicLayerTest
    {
        // logic block to test
        private LogicProcessor logicBlock;

        // mock vehicle used to test the bus
        private MockVehicle mockVehicle;

        // flag to test if the required event is captured
        private bool IsEventTriggered { get; set; }

        // fake timer to test the internal logic
        private System.Timers.Timer fakeTimer;

        // timer events count - used to stop timer to end test
        private int totalTimerEventsCount;

        [SetUp]
        public void SetupBlock()
        {
            logicBlock = new LogicProcessor
                {
                    ArriveThreshold = new TimeSpan(0, 0, 3),
                    DepartThreshold = new TimeSpan(0, 0, 5),
                    NoiseThreshold = new TimeSpan(0, 0, 10),
                    Id = 1000,
                    MarkerSerialNumber = "Test Marker",
                    LogicBuffer = new BlockingCollection<IRampActiveTag>()
                };
            IsEventTriggered = false;
            logicBlock.ArrivedEvent += logicBlock_ArrivedEvent;

            totalTimerEventsCount = 0;


            mockVehicle = new MockVehicle();

        }

        private void fakeTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            totalTimerEventsCount++;
            logicBlock.ProcessManagedTags();
        }

        // location logic arrived event
        private void logicBlock_ArrivedEvent(object sender, string e)
        {
            IsEventTriggered = true;
        }

        [Test]
        public void TagDrivesPastTheStand()
        {
            fakeTimer = new System.Timers.Timer(200);
            fakeTimer.Elapsed += fakeTimer_Elapsed;
            
            var tagsReceived = mockVehicle.DriveByStand(DateTime.Now, logicBlock);

            foreach (var rampActiveTag in tagsReceived)
            {
                logicBlock.LogicBuffer.Add(rampActiveTag);
            }
            
            logicBlock.LogicBuffer.CompleteAdding();

            logicBlock.ProcessLogicBuffer();

            fakeTimer.Start();

            while (totalTimerEventsCount < 50)
            {
                // wait for the timers to expire 50 * 200 = 10000 [10 second]
            }

            fakeTimer.Stop();

            // there should be no arrival events
            Assert.IsFalse(IsEventTriggered);
        }

        [Test]
        public void TagArrivesAtTheStand()
        {
            fakeTimer = new System.Timers.Timer(200);
            fakeTimer.Elapsed += fakeTimer_Elapsed;

            var tagsReceived = mockVehicle.ArriveAtStand(logicBlock, DateTime.Now);

            foreach (var rampActiveTag in tagsReceived)
            {
                logicBlock.LogicBuffer.Add(rampActiveTag);

            }
            logicBlock.LogicBuffer.CompleteAdding();


            logicBlock.ProcessLogicBuffer();

            fakeTimer.Start();

            while (totalTimerEventsCount < 50)
            {
                // wait for the timers to expire 50 * 200 = 10000 [10 second]
            }

            fakeTimer.Stop();

            Assert.IsTrue(IsEventTriggered);
        }
    }

}
