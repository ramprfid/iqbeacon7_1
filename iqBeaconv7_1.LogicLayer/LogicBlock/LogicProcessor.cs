﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Timers;
using iqBeacon7_1.Logger;
using iqBeaconv7_1.ReaderLayer.Tag;

namespace iqBeaconv7_1.LogicLayer.LogicBlock
{
    public class LogicProcessor : ILogic, IScheduler
    {
        public int Id { get; set; }

        public TimeSpan ArriveThreshold { get; set; }

        public TimeSpan DepartThreshold { get; set; }

        public TimeSpan NoiseThreshold { get; set; }

        public string MarkerSerialNumber { get; set; }

        public IList<IRampActiveTag> ManagedTags { get; set; }

        public BlockingCollection<IRampActiveTag> LogicBuffer { get; set; }

        public event EventHandler<string> ArrivedEvent;

        private System.Timers.Timer LogicTimer { get; set; }

        public LogicProcessor()
        {
            ManagedTags = new List<IRampActiveTag>();
            // configure logic timer to time out every 200 ms to process the ManagedQueue
            LogicTimer = new System.Timers.Timer(200);
            LogicTimer.Elapsed += LogicTimer_Elapsed;
        }

        private void LogicTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            ProcessManagedTags();
        }

        public void ProcessLogicBuffer()
        {
            // BlockingCollection is used to avoid needless polling for new tag data 
            // refer solution http://stackoverflow.com/questions/26643737/multi-thread-c-sharp-application-hitting-high-cpu-usage

            foreach (var rampActiveTag in LogicBuffer.GetConsumingEnumerable())
            {
                ProcessNewTagReceivedLogic(rampActiveTag);
            }

        }

        /// <summary>
        /// Check if the new tag exists in the Managed Tag List
        /// If No, add the new tag
        /// If yes, update the Last Confirmed Arrival Time
        /// </summary>
        /// <param name="tag"></param>
        private void ProcessNewTagReceivedLogic(IRampActiveTag tag)
        {
            try
            {
                var existingTag = ManagedTags.FirstOrDefault(x => x.SerialNumber == tag.SerialNumber);
                if (existingTag == null)
                {
                    // we need to be able to filter out tags that have been previously marked by the position marker
                    // if we find that a Time First Seen - New Loop Time is greater than 15 secs, we know that the marking is quite old 
                    // this marking can be safely ignored
                    if (tag.TimeLastSeen - tag.NewLoopTime >= DepartThreshold)
                    {
                        return;
                    }

                    // set the first read time from the NewLoopTime when the tag is first visible
                    tag.FirstReadTime = tag.NewLoopTime;

                    // the last confirmed time is the same as the first read time
                    tag.LastConfirmedReadTime = tag.FirstReadTime;

                    tag.IsTagLeftMarkerZone = false;

                    ManagedTags.Add(tag);

                    Console.WriteLine("Tag {0} added to Managed Tags list. Time Last seen: {1} New Loop Time {2}", tag.SerialNumber, tag.TimeLastSeen, tag.NewLoopTime);
                }
                else
                {
                    // there are scenarios where the tags [already tracked] float in and out of the Marker zone
                    // we need to check for those tags and update the arrival time to the new time, so arrival events are not incorrectly raised
                    if (tag.TimeLastSeen - tag.NewLoopTime > new TimeSpan(0, 0, 2))
                    {

                        Console.WriteLine("Tag left marker zone");
                        existingTag.IsTagLeftMarkerZone = true;
                        return;
                    }

                    // update the last confirmed time for the registered tag with the New Loop Time
                    // in order to track the time while the tag is in the Marker field
                    // only if the New Loop Time is more than the last confirmed time [This will cause the redundant tag reads to be ignored]
                    if (tag.NewLoopTime > existingTag.LastConfirmedReadTime)
                    {
                        if (existingTag.IsTagLeftMarkerZone)
                        {
                            existingTag.FirstReadTime = tag.NewLoopTime;
                            existingTag.LastConfirmedReadTime = tag.NewLoopTime;
                            existingTag.IsTagLeftMarkerZone = false;
                            return;
                        }
                        Console.WriteLine("Updating Last Confirmed Read Time for {0}", tag.SerialNumber);
                        existingTag.LastConfirmedReadTime = tag.NewLoopTime;
                    }

                }
            }
            catch (Exception ex)
            {
                this.AddToLog(IqBeaconLogType.Error, string.Format("Error occured when processing tag read from queue. Exception is : {0}", ex));
            }
        }

        public void ProcessManagedTags()
        {
            try
            {
                IList<uint> tagsToRemove = new List<uint>();
                foreach (IRampActiveTag tag in ManagedTags)
                {
                    // check if the tag arrival needs to be sent
                    if (ProcessTagArriveLogic(tag))
                    {
                        // jump to the next tag in the list
                        continue;
                    }

                    // check if the tag Depart needs to be sent
                    if (ProcessTagDepartLogic(tag))
                    {
                        // the tag departed message has been sent, remove the tag from the Managed List
                        tagsToRemove.Add(tag.SerialNumber);
                    }

                    if (ProcessTagNoiseLogic(DateTime.Now, tag))
                    {
                        //remove the tag that was marked as a noise tag
                        tagsToRemove.Add(tag.SerialNumber);
                    }

                    // remove flagged tags from the list of managed tags
                    RemoveFlaggedTagsFromManagedList(tagsToRemove);
                }
            }
            catch (Exception)
            {
                // it is safe to ignore exceptions here
            }
        }

        private void RemoveFlaggedTagsFromManagedList(IList<uint> tagsToRemove)
        {
            if (tagsToRemove.Count == 0) return;

            foreach (var tagId in tagsToRemove)
            {
                // get tag that matches the serial number of the tag
                var tag = ManagedTags.FirstOrDefault(x => x.SerialNumber == tagId);
                if (tag != null)
                {
                    // we need to replace the console flash with a log entry to ignore the noise marking reads
                    Console.BackgroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("Tag {0} has been removed from the list {1}.", tag.SerialNumber, MarkerSerialNumber);
                    Console.ResetColor();

                    // remove tag from Managed Tag list
                    ManagedTags.Remove(tag);
                }
            }
        }

        private bool ProcessTagNoiseLogic(DateTime currentTime, IRampActiveTag rampActiveTag)
        {
            // get the time for which the tag has been in the marker field
            var fieldTime = rampActiveTag.LastConfirmedReadTime - rampActiveTag.FirstReadTime;

            // get the total time elapsed since the first read
            var elapsedTime = currentTime - rampActiveTag.FirstReadTime;

            // we can say that a tag marked is a noise event, if the field time of the tag is less than the Arrival Threshold
            // but the elapsed time is more than the noise threshold, as we have not had a positive tag read even after we have allowed some level of latency
            if ((fieldTime < ArriveThreshold) && (elapsedTime > NoiseThreshold))
            {
                return true;
            }
            return false;
        }

        private bool ProcessTagDepartLogic(IRampActiveTag rampActiveTag)
        {
            // if the tag arrival message hasn't been sent then return 
            if (!rampActiveTag.IsTagArriveMessageSent) return false;

            // the tag arrival message has already been sent 
            // get the last known message received duration
            var lastKnownDuration = DateTime.Now - rampActiveTag.LastConfirmedReadTime;

            // check if the last confirmed arrival message was received
            if (lastKnownDuration >= DepartThreshold)
            {
                // the tag has departed, we need to send a departed message, 

                // we need to replace the console flash with the sender layer logic to send arrival message to the end point
                Console.BackgroundColor = ConsoleColor.Red;
                Console.WriteLine("Tag {0} has departed the Marker field for Marker {1}", rampActiveTag.SerialNumber, MarkerSerialNumber);
                Console.ResetColor();
                return true;
            }
            return false;
        }

        private bool ProcessTagArriveLogic(IRampActiveTag rampActiveTag)
        {
            // get the time for which the tag has been in the marker field
            var fieldTime = rampActiveTag.LastConfirmedReadTime - rampActiveTag.FirstReadTime;

            if (fieldTime == null)
            {
                throw new Exception("The tag arrival and last confirmed arrival times are not in synch....");
            }

            // check if the fieldTime is enough to pass the Arrival Threshold time
            if (fieldTime >= ArriveThreshold && !rampActiveTag.IsTagArriveMessageSent)
            {
                // the tag has arrived [set the tag arrival message flag]
                rampActiveTag.IsTagArriveMessageSent = true;
                // we need to replace the console flash with the sender layer logic to send arrival message to the end point
                Console.BackgroundColor = ConsoleColor.Green;
                Console.WriteLine("Tag {0} has Arrived in the Marker field for Marker {1}", rampActiveTag.SerialNumber, MarkerSerialNumber);
                Console.ResetColor();
                if (ArrivedEvent != null)
                    ArrivedEvent(this, "Arrived");
                return true;
            }

            return false;

        }

        public void Start()
        {
            LogicTimer.Enabled = true;
        }

        public void Stop()
        {
            LogicTimer.Enabled = false;
        }

    }
}
