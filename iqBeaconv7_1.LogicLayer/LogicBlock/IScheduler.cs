﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iqBeaconv7_1.LogicLayer.LogicBlock
{
    public interface IScheduler
    {
        void Start();

        void Stop();

    }
}
