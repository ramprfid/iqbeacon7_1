﻿using System.Configuration;

namespace iqBeaconv7_1.LogicLayer.ConfigFile
{
    public class LocationCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new LocationElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((LocationElement)element).Id;
        }
    }
}
