﻿using System;

namespace iqBeaconv7_1.LogicLayer
{
    public static class Utility
    {
        public static DateTime TrimMilliseconds(this DateTime value)
        {
            return new DateTime(value.Year, value.Month, value.Day,
                value.Hour, value.Minute, value.Second);
        }
    }
}
